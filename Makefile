default_target: generate_assets start

clean_assets:
	@make -C assets/sass clean

fetch_assets_deps: clean_assets
	@make -C assets/sass fetch

generate_assets:
	@make -C assets/sass generate

clean:
	@mint clean

install:
	@mint install

format:
	@mint format

start:
	@mint start

test:
	@echo "Test with chromium"
	@mint test -b chrome
	@echo "Test with firefox"
	@mint test -b firefox
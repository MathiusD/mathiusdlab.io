enum TextToken {
  PlainText(String)
  URL(String)
  Link(String, String)
  BreakLine(Maybe(Number))
  MultiplesToken(Array(Maybe(TextToken)))
}

module TextToken {
  fun decodeText (text : String) : Result(Object.Error, Maybe(TextToken)) {
    if (String.isEmpty(text)) {
      Result.ok(Maybe::Nothing)
    } else if (String.size(String.chopStart("\n", text)) == 0) {
      Result.ok(Maybe::Just(TextToken::BreakLine(Maybe::Just(String.size(text)))))
    } else if (String.contains("\n", text)) {
      try {
        tokens =
          for (token of String.split("\n", text)) {
            if (String.isEmpty(token)) {
              [
                Maybe::Just(TextToken::BreakLine(Maybe::Nothing))
              ]
            } else {
              [
                Maybe::Just(TextToken::PlainText(token)),
                Maybe::Just(TextToken::BreakLine(Maybe::Nothing))
              ]
            }
          }

        Result.ok(Maybe::Just(TextToken::MultiplesToken(Array.concat(tokens))))
      }
    } else {
      Result.ok(Maybe::Just(TextToken::PlainText(text)))
    }
  }

  fun decode (object : Object) : Result(Object.Error, Maybe(TextToken)) {
    try {
      text =
        object
        |> Object.Decode.string

      decodeText(text)
    } catch Object.Error => isntString {
      // Object provided isn't string switch to detection from array
      try {
        tokens =
          object
          |> Object.Decode.array(TextToken.decode)

        Result.ok(Maybe::Just(TextToken::MultiplesToken(tokens)))
      } catch Object.Error => isntArray {
        // Object provided isn't string switch to detection from object with type
        try {
          type =
            object
            |> Object.Decode.field("type", Object.Decode.string)

          case (type) {
            "plainText" =>
              try {
                text =
                  object
                  |> Object.Decode.field("text", Object.Decode.string)

                Result.ok(Maybe::Just(TextToken::PlainText(text)))
              } catch Object.Error => error {
                Result.error(error)
              }

            "url" =>
              try {
                url =
                  object
                  |> Object.Decode.field("url", Object.Decode.string)

                Result.ok(Maybe::Just(TextToken::URL(url)))
              } catch Object.Error => error {
                Result.error(error)
              }

            "link" =>
              try {
                url =
                  object
                  |> Object.Decode.field("url", Object.Decode.string)

                name =
                  object
                  |> Object.Decode.field("name", Object.Decode.string)

                Result.ok(Maybe::Just(TextToken::Link(url, name)))
              } catch Object.Error => error {
                Result.error(error)
              }

            "break" =>
              try {
                nb =
                  object
                  |> Object.Decode.field("nb", Object.Decode.number)

                if (nb > 1) {
                  Result.ok(Maybe::Just(TextToken::BreakLine(Maybe::Just(nb))))
                } else if (nb == 1) {
                  Result.ok(Maybe::Just(TextToken::BreakLine(Maybe::Nothing)))
                } else {
                  Result.ok(Maybe::Nothing)
                }
              } catch Object.Error => error {
                Result.ok(Maybe::Just(TextToken::BreakLine(Maybe::Nothing)))
              }

            "multiples" =>
              try {
                tokens =
                  object
                  |> Object.Decode.field("tokens", Object.Decode.array(TextToken.decode))

                Result.ok(Maybe::Just(TextToken::MultiplesToken(tokens)))
              } catch Object.Error => error {
                Result.error(error)
              }

            =>
              try {
                Debug.log("TextToken with following type : #{type} isn't managed")
                Result.ok(Maybe::Nothing)
              }
          }
        } catch Object.Error => error {
          Result.error(error)
        }
      }
    }
  }
}

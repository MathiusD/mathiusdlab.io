record Image {
  url : String,
  alternativeName : Maybe(String),
  targetUrl : Maybe(String)
}

module Image {
  fun decode (object : Object) : Result(Object.Error, Image) {
    try {
      url =
        object
        |> Object.Decode.field("url", Object.Decode.string)

      alternativeName =
        object
        |> Object.Decode.field("alternativeName", Object.Decode.maybe(Object.Decode.string))

      targetUrl =
        object
        |> Object.Decode.field("targetUrl", Object.Decode.maybe(Object.Decode.string))

      Result.ok(Image(url, alternativeName, targetUrl))
    } catch Object.Error => error {
      Result.error(error)
    }
  }
}

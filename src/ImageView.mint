component ImageView {
  property image : Image

  style image {

  }

  fun imgRender : Html {
    <img::image
      src={image.url}
      alt={
        case (image.alternativeName) {
          Maybe::Just(alt) => alt
          Maybe::Nothing => ""
        }
      }/>
  }

  fun render : Html {
    <{
      case (image.targetUrl) {
        Maybe::Just(targetUrl) =>
          <a href={targetUrl}>
            <{ imgRender() }>
          </a>

        Maybe::Nothing => <{ imgRender() }>
      }
    }>
  }
}

record Issue {
  name : String,
  description : Maybe(TextToken),
  img : Maybe(Image),
  subItems : Maybe(Array(Item)),
  isHidden : Bool
}

module Issue {
  fun decode (object : Object) : Result(Object.Error, Issue) {
    try {
      name =
        object
        |> Object.Decode.field("name", Object.Decode.string)

      description =
        object
        |> Object.Decode.field(
          "description",
          Object.Decode.maybe(
            TextToken.decode))

      img =
        object
        |> Object.Decode.field(
          "img",
          Object.Decode.maybe(
            Image.decode))

      subItems =
        object
        |> Object.Decode.field(
          "subItems",
          Object.Decode.maybe(
            Object.Decode.array(
              Item.decode)))

      isHidden =
        object
        |> Object.Decode.field(
          "isHidden",
          Object.Decode.maybe(
            Object.Decode.boolean))

      Result.ok(
        Issue(
          name,
          Maybe.flatten(description),
          img,
          subItems,
          Maybe.withDefault(false, isHidden)))
    } catch Object.Error => error {
      Result.error(error)
    }
  }
}

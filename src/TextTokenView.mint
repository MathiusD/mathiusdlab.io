component TextTokenView {
  property textToken : TextToken

  style textToken {

  }

  fun render : Html {
    <span::textToken>
      <{
        case (textToken) {
          TextToken::PlainText(text) => <{ text }>

          TextToken::URL(url) =>
            <a href={url}>
              <{ url }>
            </a>

          TextToken::Link(url, name) =>
            <a href={url}>
              <{ name }>
            </a>

          TextToken::BreakLine(potentielNb) =>
            case (potentielNb) {
              Maybe::Just(nb) =>
                <{
                  for (iteration of Array.range(0, nb - 1)) {
                    <br/>
                  }
                }>

              Maybe::Nothing => <br/>
            }

          TextToken::MultiplesToken(tokens) =>
            <{
              for (token of tokens) {
                case (token) {
                  Maybe::Just(tokenValue) => <TextTokenView textToken={tokenValue}/>
                  Maybe::Nothing => <{  }>
                }
              }
            }>
        }
      }>
    </span>
  }
}

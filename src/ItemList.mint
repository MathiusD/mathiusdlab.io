component ItemList {
  property nameProvided : Maybe(String)
  property items : Array(Item)

  style list {

  }

  fun render : Html {
    <div::list>
      <{
        case (nameProvided) {
          Maybe::Just(name) =>
            <{
              <h2>
                <{ name }>
              </h2>
            }>

          Maybe::Nothing => <{  }>
        }
      }>

      <ul>
        <{
          for (item of items) {
            <li>
              <{
                case (item) {
                  Item::Issue(issue) => <IssueView issue={issue}/>

                  Item::TextToken(maybeTextToken) =>
                    <{
                      case (maybeTextToken) {
                        Maybe::Just(textToken) =>
                          <{
                            <p>
                              <TextTokenView textToken={textToken}/>
                            </p>
                          }>

                        Maybe::Nothing => <{  }>
                      }
                    }>
                }
              }>
            </li>
          }
        }>
      </ul>
    </div>
  }
}

enum Item {
  Issue(Issue)
  TextToken(Maybe(TextToken))
}

module Item {
  fun decode (object : Object) : Result(Object.Error, Item) {
    try {
      issue =
        Issue.decode(object)

      Result.ok(Item::Issue(issue))
    } catch Object.Error => isntIssue {
      // Object provided isn't Issue switch to detection from TextToken
      try {
        textToken =
          TextToken.decode(object)

        Result.ok(Item::TextToken(textToken))
      } catch Object.Error => error {
        Result.error(error)
      }
    }
  }
}

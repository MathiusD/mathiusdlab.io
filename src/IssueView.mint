component IssueView {
  property issue : Issue

  state isHidden : Bool = issue.isHidden

  style issue {

  }

  fun descriptionIsPresent : Bool {
    case (issue.description) {
      Maybe::Just(description) => true
      Maybe::Nothing => false
    }
  }

  fun toggleVisibility : Promise(Never, Void) {
    next { isHidden = !isHidden }
  }

  fun render : Html {
    <div::issue
      class={
        if (isHidden || descriptionIsPresent()) {
          ""
        } else {
          "dropdown is-hoverable"
        }
      }>

      <h3
        class={
          if (isHidden || descriptionIsPresent()) {
            ""
          } else {
            "dropdown-trigger"
          }
        }
        onClick={toggleVisibility}>

        <{ issue.name }>

      </h3>

      <{
        if (!isHidden) {
          <{
            <{
              case (issue.description) {
                Maybe::Just(description) =>
                  <p>
                    <TextTokenView textToken={description}/>
                  </p>

                Maybe::Nothing =>
                  <div class="dropdown-menu">
                    <div class="dropdown-content">
                      <div class="dropdown-item notification is-danger">
                        "No description provided"
                      </div>
                    </div>
                  </div>
              }
            }>

            <{
              case (issue.img) {
                Maybe::Just(img) => <ImageView image={img}/>
                Maybe::Nothing => <{  }>
              }
            }>

            <{
              case (issue.subItems) {
                Maybe::Just(subItems) =>
                  <{
                    <ItemList
                      nameProvided={Maybe::Nothing}
                      items={subItems}/>
                  }>

                Maybe::Nothing => <{  }>
              }
            }>
          }>
        } else {
          <{  }>
        }
      }>

    </div>
  }
}

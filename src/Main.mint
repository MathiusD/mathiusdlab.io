component Main {
  state loading = false
  state error = Maybe::Nothing
  state items = Maybe::Nothing

  style app {

  }

  fun load (url : String) {
    sequence {
      next { loading = true }
      Debug.log("Starting load of items located at #{url}")

      response =
        url
        |> Http.get()
        |> Http.send()

      if (response.status == 200) {
        sequence {
          body =
            Json.parse(response.body)

          itemsParsed =
            Object.Decode.array(
              Item.decode,
              body)

          next { items = Maybe::Just(itemsParsed) }
        } catch String => jsonError {
          sequence {
            next { error = Maybe::Just("Following error occured during parsing of items : #{jsonError}") }
          }
        } catch Object.Error => rawError {
          sequence {
            next { error = Maybe::Just("Following error occured : #{Object.Error.toString(rawError)}") }
          }
        }
      } else {
        next { error = Maybe::Just("Expected HTTP Status OK (200) but receive #{response.status} with following content : #{response.body}") }
      }
    } catch Http.ErrorResponse => httpError {
      sequence {
        next
          {
            error =
              case (httpError.type) {
                Http.Error::Aborted => Maybe::Just("Loading of items are aborted by client.")
                Http.Error::BadUrl => Maybe::Just("URL of items are malformated (URL related : #{httpError.url}).")
                Http.Error::NetworkError => Maybe::Just("Error occured during fetch of items (Status code : #{httpError.status}).")
                Http.Error::Timeout => Maybe::Just("Items location cannot be reached.")
              }
          }
      }
    } finally {
      sequence {
        Debug.log(
          case (error) {
            Maybe::Just(error) => error
            Maybe::Nothing => "Items loaded !"
          })

        next { loading = false }
      }
    }
  }

  fun componentDidMount : Promise(Never, Void) {
    load(@asset(../assets/items.json))
  }

  fun render : Html {
    <div::app class="content">
      <{
        if (loading) {
          <div class="notification is-info">
            <div>
              <{ "Loading in progress..." }>
            </div>

            <progress
              class="progress"
              max="100"/>
          </div>
        } else {
          <{  }>
        }
      }>

      <{
        case (error) {
          Maybe::Just(errorMsg) =>
            <div class="notification is-danger">
              <{ errorMsg }>
            </div>

          Maybe::Nothing => <{  }>
        }
      }>

      <ItemList
        nameProvided={Maybe::Just("MathiusD public TODO list")}
        items={Maybe.withDefault([], items)}/>
    </div>
  }
}

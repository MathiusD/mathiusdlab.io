suite "TextToken Decoding" {
  test "Test TextToken decoding well PlainText from String" {
    with Test.Context {
      of("\"example\"")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::PlainText("example"))
    }
  }

  test "Test TextToken decoding well PlainText from Object" {
    with Test.Context {
      of("{\"type\": \"plainText\", \"text\": \"example\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::PlainText("example"))
    }
  }

  test "Test TextToken decoding well Url from Object" {
    with Test.Context {
      of("{\"type\": \"url\", \"url\": \"foo.org\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::URL("foo.org"))
    }
  }

  test "Test TextToken decoding well Link from Object" {
    with Test.Context {
      of("{\"type\": \"link\", \"url\": \"foo.org\", \"name\": \"example\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::Link("foo.org", "example"))
    }
  }

  test "Test TextToken decoding well single BreakLine from String" {
    with Test.Context {
      of("\n")
      |> map(TextToken.decodeText)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::BreakLine(Maybe::Just(1)))
    }
  }

  test "Test TextToken decoding well multiples BreakLine from String" {
    with Test.Context {
      of("\n\n")
      |> map(TextToken.decodeText)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::BreakLine(Maybe::Just(2)))
    }
  }

  test "Test TextToken decoding well single BreakLine from Object without number of break" {
    with Test.Context {
      of("{\"type\": \"break\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::BreakLine(Maybe::Nothing))
    }
  }

  test "Test TextToken decoding well single BreakLine from Object with positive number of break" {
    with Test.Context {
      of("{\"type\": \"break\", \"nb\": 1}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::BreakLine(Maybe::Nothing))
    }
  }

  test "Test TextToken decoding well multiples BreakLine from Object" {
    with Test.Context {
      of("{\"type\": \"break\", \"nb\": 2}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(TextToken.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Maybe(TextToken))) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertOf(true, Maybe.isJust)
      |> then(
        (maybe : Maybe(TextToken)) {
          case (maybe) {
            Maybe::Just(value) => Promise.resolve(value)
            Maybe::Nothing => Promise.reject(maybe)
          }
        })
      |> assertEqual(TextToken::BreakLine(Maybe::Just(2)))
    }
  }
}

suite "Image Decoding" {
  test "Test Image decoding well with only url" {
    with Test.Context {
      of("{\"url\": \"foo.org\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertEqual(Image("foo.org", Maybe::Nothing, Maybe::Nothing))
    }
  }

  test "Test Image decoding well with only url and ignore other params" {
    with Test.Context {
      of("{\"url\": \"foo.org\", \"john\": \"doe\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertEqual(Image("foo.org", Maybe::Nothing, Maybe::Nothing))
    }
  }

  test "Test Image decoding well with url and alternativeName" {
    with Test.Context {
      of("{\"url\": \"foo.org\", \"alternativeName\": \"example\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertEqual(Image("foo.org", Maybe::Just("example"), Maybe::Nothing))
    }
  }

  test "Test Image decoding well with url and targetUrl" {
    with Test.Context {
      of("{\"url\": \"foo.org\", \"targetUrl\": \"foo.org\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertEqual(Image("foo.org", Maybe::Nothing, Maybe::Just("foo.org")))
    }
  }

  test "Test Image decoding well with url, alternativeName and targetUrl" {
    with Test.Context {
      of("{\"url\": \"foo.org\", \"alternativeName\": \"example\", \"targetUrl\": \"foo.org\"}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> assertEqual(Image("foo.org", Maybe::Just("example"), Maybe::Just("foo.org")))
    }
  }

  test "Test Image decoding fail because url isn't found" {
    with Test.Context {
      of("{}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isError)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.reject(value)
            Result::Err(error) => Promise.resolve(error)
          }
        })
    }
  }

  test "Test Image decoding fail because url isn't found" {
    with Test.Context {
      of("{}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isError)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.reject(value)
            Result::Err(error) => Promise.resolve(error)
          }
        })
    }
  }

  test "Test Image decoding fail because url is an Object" {
    with Test.Context {
      of("{\"url\": {}}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isError)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.reject(value)
            Result::Err(error) => Promise.resolve(error)
          }
        })
    }
  }

  test "Test Image decoding fail because url is an Array" {
    with Test.Context {
      of("{\"url\": []}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isError)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.reject(value)
            Result::Err(error) => Promise.resolve(error)
          }
        })
    }
  }

  test "Test Image decoding fail because url is a Number" {
    with Test.Context {
      of("{\"url\": 5}")
      |> map(Json.parse)
      |> assertOf(true, Result.isOk)
      |> then(
        (result : Result(String, Object)) {
          case (result) {
            Result::Ok(value) => Promise.resolve(value)
            Result::Err(error) => Promise.reject(error)
          }
        })
      |> map(Image.decode)
      |> assertOf(true, Result.isError)
      |> then(
        (result : Result(Object.Error, Image)) {
          case (result) {
            Result::Ok(value) => Promise.reject(value)
            Result::Err(error) => Promise.resolve(error)
          }
        })
    }
  }
}

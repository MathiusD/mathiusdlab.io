suite "ImageView" {
  test "ImageView must be generate root img with only url defined" {
    with Test.Html {
      <ImageView image={Image("foo.org", Maybe::Nothing, Maybe::Nothing)}/>
      |> start()
      |> assertElementExists("img")
    }
  }

  test "ImageView must be generate root img with url and alternativeName defined" {
    with Test.Html {
      <ImageView image={Image("foo.org", Maybe::Just("example"), Maybe::Nothing)}/>
      |> start()
      |> assertElementExists("img")
    }
  }

  test "ImageView must be generate root a and img as child with url and targetUrl defined" {
    with Test.Html {
      <ImageView image={Image("foo.org", Maybe::Nothing, Maybe::Just("foo.org"))}/>
      |> start()
      |> assertElementExists("a")
      |> find("a")
      |> assertElementExists("img")
    }
  }

  test "ImageView must be generate root a and img as child with url, alternativeName and targetUrl defined" {
    with Test.Html {
      <ImageView image={Image("foo.org", Maybe::Just("example"), Maybe::Just("foo.org"))}/>
      |> start()
      |> assertElementExists("a")
      |> find("a")
      |> assertElementExists("img")
    }
  }
}
